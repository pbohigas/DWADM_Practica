export class Sighting {

  id: number;
  idBird: number;
  placeDescription: string;
  longitude: string;
  latitude: string;

  constructor(id, idBird, placeDescription, longitude, latitude){

    this.id = id;
    this.idBird = idBird;
    this.placeDescription = placeDescription;
    this.longitude = longitude;
    this.latitude = latitude;

  }

}
