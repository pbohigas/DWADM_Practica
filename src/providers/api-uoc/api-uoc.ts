import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

interface LoginResponse {
  status: string
  id: string
  message: string
}

interface AddSightingResponse {
  status: string
  message: string
}

@Injectable()
export class ApiUocProvider {

  loginURL: string = 'http://dev.contanimacion.com/birds/public/login/';
  getBirdsURL: string = 'http://dev.contanimacion.com/birds/public/getBirds/';
  getBirdDetailsURL: string = 'http://dev.contanimacion.com/birds/public/getBirdDetails/';
  addSightingURL: string = 'http://dev.contanimacion.com/birds/public/getBirdDetails/';

  constructor(public http: HttpClient) {
    console.log('Created an ApiUoc provider');
  }

  login(email: string, password: string) {
    console.log("Sending the post request 'login_UOC' to '" + this.loginURL + "'");
    return this.http.post<LoginResponse>(this.loginURL, {"email": email, "password": password});
  }

  getBirds(userId: number) {
    console.log("Sending the get request 'getBirds' to '" + this.getBirdsURL + "'");
    return this.http.get<any[]>(this.getBirdsURL + userId);
  }

  getBirdDetails(birdId: number) {
    console.log("Sending the get request 'getBirdDetails' to '" + this.getBirdsURL + "'");
    return this.http.get<any[]>(this.getBirdDetailsURL + birdId);
  }

  addSighting(birdId: number, placeDescription: string, latitude: number, longitude: number) {
    console.log("Sending the post request 'addSighting' to '" + this.addSightingURL + "'");
    console.log({"idAve": birdId, "place": placeDescription, "long": longitude, "lat": latitude});
    return this.http.post<AddSightingResponse>(this.addSightingURL, {"idAve": birdId, "place": placeDescription, "long": longitude, "lat": latitude});
  }

}
