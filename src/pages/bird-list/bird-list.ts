import {Component} from '@angular/core';
import {IonicPage, Loading, LoadingController, NavController, NavParams} from 'ionic-angular';
import {ApiUocProvider} from "../../providers/api-uoc/api-uoc";
import {Bird} from "../../app/Bird";

@IonicPage()
@Component({
  selector: 'page-bird-list',
  templateUrl: 'bird-list.html',
})

export class BirdListPage {

  private userId: number;
  private loading: Loading;
  private waitingMsgBirdList = "Loading bird list ...";
  private birdList: Array<Bird> = [];


  constructor(public navCtrl: NavController, public navParams: NavParams, public apiUOC: ApiUocProvider, private loadingCtrl: LoadingController) {
    this.userId = navParams.get('userId');
    console.log("create page");
  }

  ionViewDidLoad() {
    this.loadBirdList(this.userId);
    console.log("load page");
  }

  private loadBirdList(userId: number) {

    console.log(this.waitingMsgBirdList);
    this.showLoading();

    let loadOrder = 0;

    this.apiUOC.getBirds(userId)
      .subscribe(
        data => {

          data.forEach(bird => {

            var newBird = new Bird(bird.id, bird.bird_name, bird.bird_image, bird.bird_sightings, bird.bird_mine);
            this.birdList.push(newBird);

            console.log("*******************************");
            console.log("Load bird " + ++loadOrder);
            console.log("\tId: " + newBird.id.toString());
            console.log("\tName: " + newBird.name.toString());
            console.log("\tImage's URL: " + newBird.imageSrc.toString());
            console.log("\tGlobal sightings: " + newBird.globalSightings.toString());
            console.log("\tUser's sightings: " + newBird.userSightings.toString());

          });
        },
        err => {
          console.log("Error on the request")
        }
      );
    this.loading.dismiss();

  }

  private showLoading() {
    this.loading = this.loadingCtrl.create({
      content: this.waitingMsgBirdList,
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  private selectBird(idBird: number) {
    this.navCtrl.push('BirdDetailsPage', {idBird: idBird});
  }

  hasUserSightings(userSightings: number) {
    if (userSightings > 0) {
      return true;
    } else {
      return false;
    }
  }
}
