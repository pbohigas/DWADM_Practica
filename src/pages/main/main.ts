import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})

export class MainPage {

  userId: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController) {
    this.userId = navParams.get('userId');
  }

  ionViewDidLoad() {
    console.log("Show MainPage");
    console.log("Main Page - " + this.userId);
  }

  private showBirdList() {
    this.navCtrl.push('BirdListPage', {userId: this.userId});
  }

  private showAddBird() {
    this.navCtrl.push('AddBirdPage');
  }

  private showInfo() {
    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: 'This content is not available',
      buttons: ['Ok']
    });
    alert.present();
  }

  private disconnect() {
    this.userId = null;
    this.navCtrl.setRoot('LoginPage');
  }

}
